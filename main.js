// Дещо незрозуміле ТЗ. Якщо мається на увазі те, що 0 - НЕ натуральне число 
// і через це відлік ми починаємо з 1, то від 0 до введеного користувачем числа
// тільки 0, 1, 2, 3, 4 не діляться на 5 без остачі. Будь ласка, якщо мій варіант 
// невірний, вкажіть правильний варіант. Дякую!

let userNum = +prompt("Enter your number");


switch (userNum) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
        console.log("Sorry, no numbers")
        break;   
    }


for (let i = 0; i<= userNum; i++) {
    if (i % 5 === 0 && userNum >=5) {
    console.log(i);
    } 
}